from sopel import module
from sopel.module import rule, commands, example


@module.commands("ping", "[h|H]ello", "[h|H]i")
@module.rule("([h|H]ello)|([h|H]i)")
def hello(bot, trigger):
    if trigger.group(1) == 'ping':
        bot.reply("pong")
    else:
        bot.say("Hello, %s" % trigger.nick)

@module.commands("[g|G]oodbye", "[l|L]ater", "[b|B]ye")
@module.rule("([g|G]oodbye)|([l|L]ater)|([b|B]ye)")
def goodbye(bot, trigger):
    bot.reply("l8r %s" % trigger.nick)